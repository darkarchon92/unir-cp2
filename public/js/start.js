$('#aKnowledge').click(function() {
        $('#divKnowledge').slideToggle(500),
  $('.bar-percentage[data-percentage]').each(function () {
  var progress = $(this);
  var percentage = Math.ceil($(this).attr('data-percentage'));
  $({countNum: 0}).animate({countNum: percentage}, {
    duration: 2000,
    easing:'linear',
    step: function() {
      // What todo on every count
      var pct = Math.floor(this.countNum) + '%';
      progress.text(pct) && progress.siblings().children().css('width',pct);
    }
  });
});
});

$('#aServices').click(function() {
        $('#divServices').slideToggle(500);
});

  $(function() {
    $('a[href*="#"]:not([href="#"])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 700);
          return false;
        }
      }
    });
  });

// FORMULARIO-----------------------------------------------------------------
  //variables globales
  var inputName = $("#name");
  var reqName = $("#req-name");
  var inputEmail = $("#email");
  var reqEmail = $("#req-email");
  var inputTextArea = $("#textarea");
  var reqTextArea = $("#req-textarea");
  var reqSelect = $("#req-select");
  var inputSelect = $("#select-choice")
  var indice = $('#select-choice').find(":selected").val()

//funciones de validacion
function validateName(){
  //NO cumple longitud minima
  if(inputName.val().length < 4){
    reqName.addClass("error");
    inputName.addClass("error");
    return false;
  }
  //SI longitud pero NO solo caracteres A-z
  else if(!inputName.val().match(/^[a-zA-Z" "]+$/)){
    reqName.addClass("error");
    inputName.addClass("error");
    return false;
  }
  // SI longitud, SI caracteres A-z
  else{
    reqName.removeClass("error");
    inputName.removeClass("error");
    return true;
  }
}
function validateEmail(){
  //NO hay nada escrito
  if(inputEmail.val().length == 0){
    reqEmail.addClass("error");
    inputEmail.addClass("error");
    return false;
  }
  // SI escrito, NO VALIDO email
  else if(!inputEmail.val().match(/^[^\s()<>@,;:\/]+@\w[\w\.-]+\.[a-z]{2,}$/i)){
    reqEmail.addClass("error");
    inputEmail.addClass("error");
    return false;
  }
  // SI rellenado, SI email valido
  else{
    reqEmail.removeClass("error");
    inputEmail.removeClass("error");
    return true;
  }
}
function validateTextArea(){
  //NO cumple longitud minima
  if(inputTextArea.val().length < 30){
    reqTextArea.addClass("error");
    inputTextArea.addClass("error");
    return false;
  }
  // SI longitud
  else{
    reqTextArea.removeClass("error");
    inputTextArea.removeClass("error");
    return true;
  }
}
function validateSelect(){
  //NO cumple longitud minima
  indice = $('#select-choice').find(":selected").val()
  if( indice == null || indice == 0 ) {
    reqSelect.addClass("error");
    inputSelect.addClass("error");
    return false;
}
else{
    reqSelect.removeClass("error");
    inputSelect.removeClass("error");
    return true;
  }
}


//controlamos la validacion en los distintos eventos
// Perdida de foco
inputName.blur(validateName);
inputEmail.blur(validateEmail);
inputTextArea.blur(validateTextArea);
inputSelect.blur(validateSelect);

// Pulsacion de tecla
inputName.keyup(validateName);


// Envio de formulario
$("#form").submit(function(){
  if(validateName() & validateTextArea() & validateEmail() & validateSelect())
    return true;
  else
    return false;
});